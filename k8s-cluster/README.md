# K8s cluster
For the clusterr environment, a lightweight k8s distro is going to be used. There are several options to choose from such as:
- [Microk8s](https://microk8s.io/) it has several features out the box including private registry.
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) it the most common use for this porpose.
- [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/) it is based on docker image to run (not to be confused with the CRI).
- [K3s](https://k3s.io/) it is a light stripped down distro usefull to run k8s on a raspberry pi.
- [K0s](https://k0sproject.io/) it is also a light stripped down distro with a declarative deployment.

**k0s** is the one chosen for this case

## Deploy cluster 
Following these [instructions](https://docs.k0sproject.io/latest/k0s-single-node/), the cluster will be created as a single node.
```bash
# Run cluster on background
cd k0s
nohup sh start_k0s.sh 2>&1 &

# Get kubeconfig
sudo k0s kubeconfig create --groups "system:masters" admin > $HOME/.kube/config
```
