# Kong Ingress
[https://github.com/Kong/kubernetes-ingress-controller](https://github.com/Kong/kubernetes-ingress-controller)

* Install
```bash
helm repo add kong https://charts.konghq.com
helm repo update
helm install legion-gateway kong/kong --set ingressController.installCRDs=false
```
