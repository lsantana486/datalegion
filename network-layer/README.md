# Network Layer

It is composed by:
* [Certificate Manager](./cert-manager/README.md) in charge of managing the TLS secrets for **datalegion.dev** domain.
* [Kong](./kong-ingress/README.md) in charge of managing the access to the services.
* [Kuma](./kuma/README.md) in charge of the service mesh


