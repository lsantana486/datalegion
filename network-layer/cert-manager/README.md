# Cert Manager
[https://cert-manager.io/docs/installation/kubernetes/](https://cert-manager.io/docs/installation/kubernetes/)

* Install
```bash
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager -f values.yaml --version v1.0.3
```

* Install [webhook](./cert-manager-webhook-godaddy/README.md)

* Create Issuer
```bash
kubectl apply -f issuer.yaml
```

* Create Certificate
```bash
kubectl apply -f certificate.yaml
```