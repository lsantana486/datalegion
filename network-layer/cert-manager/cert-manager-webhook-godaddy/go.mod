module cert-manager-webhook-godaddy

go 1.15

require (
	github.com/jetstack/cert-manager v1.0.3
	k8s.io/apiextensions-apiserver v0.19.2
	k8s.io/client-go v0.19.2
	sigs.k8s.io/structured-merge-diff v0.0.0-20190525122527-15d366b2352e // indirect
)
