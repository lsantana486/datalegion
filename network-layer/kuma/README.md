# Kuma Service Mesh
[https://kuma.io/docs/0.7.2/installation/helm/](https://kuma.io/docs/0.7.2/installation/helm/)

* Install
```bash
helm repo add kuma https://kumahq.github.io/charts
helm repo update
kubectl create namespace kuma-system
helm install --namespace kuma-system kuma kuma/kuma
```

* GUI
```bash
kubectl port-forward svc/kuma-control-plane -n kuma-system 5681:5681
# http://localhost:5681/gui/
```